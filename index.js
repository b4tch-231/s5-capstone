console.log("wew");

class Customer {
  constructor(email){
    this.email = email;
    this.cart = new Cart();
    this.orders = [];   
  }

  checkOut(){
    if(this.orders.length === 0){
      this.orders.push(this.cart)
    }
    return this;
  }
}

class Product {
  constructor(name, price){
    this.name = name;
    this.price = price;
    this.isActive = true;
  }

  archive(){
    if(this.isActive === true){
      this.isActive = false;
    }
    return this;
  }

  updatePrice(updatedPrice){
    this.price = updatedPrice;
    return this;
  }
}

class Cart {
  constructor(){
    this.contents = [];
    this.totalAmount = 0;
  }

  addToCart(name, quantity){
    
    this.contents.push({
      product: name,
      quantity: quantity
    })
    return this;
  }

  showCartContents(){
    console.log(this.contents)
  }

  updateProductQuantity(name, updateQuantity){
    this.contents.find(content => 
      content.product.name === name)
  }

  clearCartContents(){
    this.contents = []
    return this;
  }

  computeTotal(){
    let totalSum = 0;
    let totalQuantity = 0;
    this.contents.forEach(content => {
      totalSum = totalSum + content.product.price
      totalQuantity = totalQuantity + content.quantity
    })
    
    this.totalAmount = totalSum * totalQuantity
    return this;
  }
}






// Customers
let customer1 = new Customer("john@mail.com");


// Products
let product1 = new Product("Mouse", 200);
let product2 = new Product("Keyboard", 600);
let product3 = new Product("Monitor", 2500);
let product4 = new Product("CPU", 5800);

customer1.cart.addToCart(product1, 3)
customer1.cart.addToCart(product2, 3)

customer1.cart.computeTotal()
// customer1.cart.updateProductQuantity('Mouse', 5)
customer1.cart.showCartContents()
customer1.checkOut()


